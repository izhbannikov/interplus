!!!WARNING: THIS PROGRAM IS UNDER TEST, SO DO NOT BE MAD IF IT FAILS ON YOUR DATA!!!

A simple wrapper for INTERSNP program. It relies on the GWAS+ pipeline developed at BARU by Igor V. Akushevich, Mikhail R. Kovtun, and others.
Contributors: Ilya Y. Zhbannikov, Liubov Arbeeva, Konstantin Arbeev, Svetlana Ukraintseva, Anatoliy I. Yashin.

Usage:

vpython main.py -pdir <your project directory> -pname <your project name> -phile <a sas file containing family and subj ids, covariates and phenotype> -pheno <phenotype of interest> -annfn <annotation file> -bfile <directory containing plink files: bed, bim, fam> -cov <a comma-separated set of covariates, without spaces> -fid <family id> -sid <subject id>

Where: main.py - a script that runs the analysis, <PROJECT> - a study name, can be CHS, HRS, FRAM_CARe, ARIC, etc., <Selection file> - a file describes the strategy for INTERSNP program. 
<SNP file> - a file that contains a target SNPs.
<Particular SNPs to keep in filtered report> - a txt file containing a list of SNPs to show in filtered report. One line per SNP.
<Number> - a test number, see original paper or INTERSNP's user manual.


Example: 

vpython main.py -pdir /data/work/iz12/interPlus/CHS -pname CHS -pfile /data/work/ka29/Project2/pipeline_source/CHS/bn_survival_1_chs_black.sas7bdat -pheno bn_survival_1_CHS -genes /data/work/iz12/interPlus/select.xls:Jazwinski -annfn /data/work/iz12/pipeline/ANNOTATION/CHS/ann5Gene.csv -bfile /data/work/iz12/pipeline/ANNOTATION/CHS/CHS -cov Sex,BirthCohort

Stratified by sex (-ss and -scov Sex):

vpython main.py -ss -scov Sex -pdir /data/work/iz12/interPlus/CHS -pname CHS -pfile /data/work/ka29/Project2/pipeline_source/CHS/bn_survival_1_chs_black.sas7bdat -pheno bn_survival_1_CHS -genes /data/work/iz12/interPlus/select.xls:Jazwinski -annfn /data/work/iz12/pipeline/ANNOTATION/CHS/ann5Gene.csv -bfile /data/work/iz12/pipeline/ANNOTATION/CHS/CHS -cov Sex,BirthCohort


How to install:

1. Copy /data/work/iz12/interPlus/ folder to the directory you like. You may skip the annotation folder (/data/work/iz12/interPlus/src/pipeline/ANNOTATION) 
since it occupies too much space and instead link it, for example: ln -s /data/work/iz12/interPlus/src/pipeline/ANNOTATION <your dir>/interPlus/src/pipeline/

2. Edit /data/work/iz12/interPlus/src/pipeline/pipeline.sh script to change covariates, sex (curently it does not hangle situation the sex="MALE \space FEMALE" is used), age if necessary.

3. Prepare selection file for INTERSNP or use the default selection strategy:

SINGLE_MARKER 1
TWO_MARKER 1
TEST 3
COVARIATES 1
END


4. Run the analysis:

vpython main.py -pdir /data/work/iz12/interPlus/CHS -pname CHS -pfile /data/work/ka29/Project2/pipeline_source/CHS/bn_survival_1_chs_black.sas7bdat -pheno bn_survival_1_CHS -genes /data/work/iz12/interPlus/select.xls:Jazwinski -annfn /data/work/iz12/pipeline/ANNOTATION/CHS/ann5Gene.csv -bfile /data/work/iz12/pipeline/ANNOTATION/CHS/CHS -cov Sex,BirthCohort

5. When the program is done performing analysis, results are located in the following directory: interPlus/<project directory>/Results/