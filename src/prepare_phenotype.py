#!/usr/bin/env vpython
#   -*- coding: utf-8 -*-


import sas7bdat
from sas7bdat import *
import pandas as pd

def prepPheno(filename, familyID, subjectID, pheno, covariates, outfile_pheno, outfile_covariates, outfile_individuals, sex, sexcov) :
	f = SAS7BDAT(filename)
	df = f.to_data_frame()
	
	# To lowercase:
	df.columns = [x.lower() for x in df.columns]
	familyID = familyID.lower()
	subjectID = subjectID.lower()
	pheno = pheno.lower()
	
	covar = covariates.split(',')
	cols = [familyID, subjectID]
	for i in range(len(covar)) :
		cols.append(covar[i].strip().lower())
	cols.append(pheno)
	
	df = df[cols]
	# Remove rows with missing phenotype, family id and subject id:
	df = df[pd.notnull(df[familyID])]
	df = df[pd.notnull(df[subjectID])]
	df = df[pd.notnull(df[pheno])]
	
	#df[familyID] = df[familyID].astype(basestring)
	#df[familyID] = df[familyID].map('{:.0f}'.format)
	df[subjectID] = df[subjectID].astype(basestring)
	df[subjectID] = df[subjectID].map('{:.0f}'.format)
	#print df.columns
	#if sex in ["MALE","FEMALE"] :
	#	df = df[pd.notnull(df[sexcov])]
	#	if sex == "MALE" :
	#		df = df[df[sexcov] == 1]
	#	else :
	#		df = df[df[sexcov] == 2]
		
	# Prepare phenotypes:
	dfpheno = df[[familyID, subjectID, pheno]]
	dfpheno[pheno] = dfpheno[pheno].astype('int')
	dfpheno.to_csv(path_or_buf=outfile_pheno, sep="\t", header=False, index=False, na_rep=".")
	
	# Prepare covariates:
	#print "AAA"
	dfcov = df[cols[:-1]]
	#print "BBB"
	dfcov_columns = ['FID', 'PID'] # Names have to be chosen from configuration file
	for i in range(len(cols)-3) :
		dfcov_columns.append('COV'+str(i+1))
	dfcov.columns = dfcov_columns
	dfcov.to_csv(path_or_buf=outfile_covariates, sep="\t", header=True, index=False, na_rep="-")
	
	# Prepare individuals:
	dfind = df[[familyID, subjectID]]
	dfind.to_csv(path_or_buf=outfile_individuals, sep="\t", header=False, index=False, na_rep=".")
	
	
