#!/usr/bin/env vpython
#-*- coding: utf-8 -*-

# Selects candidate snps using candidate genes
	
import sys
import argparse
import xlrd
import csv
import os
import itertools


def selectCandidateGenes(indata, outfile, annfn):
	
	splitted = indata.split(':')
	if len(splitted) != 2 :
		print "Error: input_filename or column not provided"
			
	genes_filename = splitted[0].strip()
	column = splitted[1].strip()
		
	select_genes(genes_filename, column, annfn, outfile)
	
def value_from_key(sheet, key):
	for row_index, col_index in product(xrange(sheet.nrows), xrange(sheet.ncols)):
		if sheet.cell(row_index, col_index).value == key:
			return sheet.cell(row_index+1, col_index).value

def colid_from_key(sheet, key):
	for col_index in xrange(sheet.ncols):
		if sheet.cell(0, col_index).value.strip() == key:
			return col_index
			
def select_genes(genes_filename, column, annfilename, outfile) :
	snps = []
	with open(annfilename, 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter='\t')
		for row in reader:
			snps.append(row)
	
	# Reading excel input file and writing into output text file:
	outhandle = open(outfile, 'w')
	
	inwb = xlrd.open_workbook(genes_filename)
	ws = inwb.sheets()[0]
	
	nrows = ws.nrows
	
	colid = colid_from_key(ws, column)
	if colid != None :
		for row in range(1, nrows) :
			for i in range(len(snps)) :
				gname = str(ws.cell(row,colid).value.strip())
				if (gname != '') and (gname in [x.strip() for x in snps[i][-1].split(',')]):
					outhandle.write(snps[i][3].strip())
					outhandle.write('\n')
			
			if str(ws.cell(row,colid).value.strip()) == "":
				break
	else :
		print "Error: can't find provided column. Aborting..."
		
	# reading candidate genes file, get the required column:
	outhandle.close()
	

	

	
if __name__ == '__main__':
    main()
    
