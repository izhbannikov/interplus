#!/usr/bin/env vpython
#   -*- coding: utf-8 -*-

def prepSelectionFile(selfname, fam, bim, bed, covariatefile, singlemarker, twomarker, test, covariates, snplist, sexcov, outputname) :
	#select = {"FAM": fam, "BIM": bim, "BED": bed, "COVARIATEFILE": covariatefile, "SINGLE_MARKER": singlemarker, "TWO_MARKER": twomarker, "TEST": test, "COVARIATES": covariates, "SNPFILE": snplist if snplist != None else "", "SNPLIST": "0" if snplist == None else "1", "SEXCOV": sexcov, "OUTPUTNAME": outputname}
	
	select = {"FAM": fam, "BIM": bim, "BED": bed, "COVARIATEFILE": covariatefile, "SINGLE_MARKER": singlemarker, "TWO_MARKER": twomarker, "TEST": test, "COVARIATES": covariates, "SNPFILE": snplist if snplist != None else "", "SNPLIST": "0" if snplist == None else "1", "OUTPUTNAME": outputname}
	f = open(selfname, 'w')
	for k,v in select.iteritems() :
		line = k + " " + v
		f.write("%s\n" % line)
	f.write("%s\n" % "END")
	f.close()
	
	
def prepSelectionFile2(selfname_in, selfname_out, fam, bim, bed, covariatefile, snplist, outputname) :
	
	select = {"FAM": fam, "BIM": bim, "BED": bed, "COVARIATEFILE": covariatefile, "SNPFILE": snplist if snplist != None else "", "SNPLIST": "0" if snplist == None else "1", "OUTPUTNAME": outputname}
	
	f = open(selfname_in, 'rU')
	lines = f.readlines()
	f.close()
	
	for line in lines :
		splits = line.split('\t')
		if len(splits) >= 2 :
			if splits[0] in select : 
				continue
			else :
				select[splits[0].strip()] = splits[1].strip().rstrip('\n')
	
	f = open(selfname_out, 'w')
	for k,v in select.iteritems() :
		line = k + "\t" + v
		f.write("%s\n" % line)
	f.write("%s\n" % "END")
	f.close()
