#!/usr/bin/env vpython
#   -*- coding: utf-8 -*-

# Prepares covariates for INTERSNP

import sas7bdat
from sas7bdat import *

"""
def prepCovFile(infile, outfile) :
	with open(infile) as f:
		content = f.readlines()
	f.close()
	
	for i in range(len(content)) :
		content[i].replace(' ', '\t')
		content[i] = content[i][:-1]
		content[i] += '\t' + '-'
	
	# Writing output file
	f = open(outfile, 'w')
	f.write("%s\n" % "FAM	PID	COV1	COV2	COV3")
	for line in content:
		f.write("%s\n" % line)
	f.close()
"""
	
def prepCovar(filename, familyID, subjectID, covariates, outfile) :
	f = SAS7BDAT(filename)
	df = f.to_data_frame()
	df.columns = map(str.lower, df.columns)
	
	# To lowercase:
	
	
	covar = covariates.split(',')
	cols = [familyID, subjectID]
	for i in range(len(covar)) :
		cols.append(covar[i].strip().lower())
		
	df_covar = df[cols]
	df_covar[subjectID] = df_covar[subjectID].astype(basestring)
	df_covar[subjectID] = df_covar[subjectID].map('{:.0f}'.format)
	df_covar.to_csv(path_or_buf=outfile, sep="\t", header=True, index=False)