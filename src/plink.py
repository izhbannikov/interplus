#!/usr/bin/env vpython
#   -*- coding: utf-8 -*-

# Performs several tasks with plink, such as quality control, select snps, etc.

import subprocess
import sas7bdat
from sas7bdat import *

def runPlink(path, phenotype, output_prefix) :
	subprocess.call(["plink", "--noweb", "--bfile", path, "--pheno", phenotype, "--1", "--make-bed", "--out", output_prefix])
	
def plinkQC(bfile, selected_snps, individuals, outpath) :
	qc = "--mind 0.05 --hwe 0.001 --maf 0.05 --geno 0.05"
	if selected_snps != None :
		subprocess.call(["plink", "--noweb", "--bfile", bfile, "--extract", selected_snps, "--keep", individuals, "--make-bed", "--out", outpath])
		subprocess.call(["plink", "--noweb", "--bfile", outpath, "--nonfounders", "--mind", "0.05", "--hwe", "0.001", "--maf", "0.05", "--geno", "0.05", "--make-bed", "--out", outpath + "_qc"])