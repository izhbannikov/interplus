#!/usr/bin/env vpython
#   -*- coding: utf-8 -*-

# A convenient wrapper for INTERSNP program

import subprocess
import sys
import argparse
import os
import ntpath
import openpyxl

# Importing custom modules
sys.path.append(os.path.dirname(os.path.realpath(__file__))+'/src')
import prepare_covariates, selection, prepare_phenotype, select_candidate_genes, plink
from prepare_covariates import *
from selection import *
from prepare_phenotype import *
from select_candidate_genes import *
from plink import *

global curr_dir
global intersnp_exec

def getProjectFolderName(project) :
	global pipeline_dir
	h = open(pipeline_dir+"tmp_"+project, 'rU')
	lines = h.readlines()
	h.close()
	return lines[0].split('/')[-1][:-1] if len(lines) != 0 else None


def runIntersnp(modelfile) : # Make a separate module for this function or a class
	subprocess.call([intersnp_exec, modelfile])

def addDescriptionSheet(filename) :
	global curr_dir
	wb = openpyxl.load_workbook(filename)
	ws = wb.create_sheet()
	ws.title = "Description"
	f = open(curr_dir + "/src/variable_description.txt")
	lines = f.readlines()
	f.close()
	for i in range(len(lines)):
		lsplit = lines[i].split(';')
		ws.append(lsplit)
	
	wb.save(filename)
		

# Starts the analysis
def main() :
	global curr_dir
	curr_dir = os.path.dirname(os.path.realpath(__file__))
	
	global intersnp_exec
	intersnp_exec = curr_dir + "/bin/INTERSNP_1.15/intersnp"

	# Setting-up options for option parser:
	usage = "main.py -p <Project name> [other options]"
	parser = argparse.ArgumentParser(usage=usage)
	parser.add_argument("-pdir", "--project_dir", action="store", type=str, dest="project_dir", default=None, required=True, help="Project directory")
	parser.add_argument("-pname", "--project_name", action="store", type=str, dest="pname", default=None, required=True, help="Name of the project")
	parser.add_argument("-s", "--selection",action="store", type=str, dest="selfname", default=None, required=False, help="Selection file name") 
	parser.add_argument("-snplist", "--snplist",action="store", type=str, dest="snplist", default=None, required=False, help="SNP list file name") 
	parser.add_argument("-ss", "--sexstrat",action="store_true", dest="sexstrat", default=False, required=False, help="Sex stratification, if set then analysis will be separately for males and females. Here: 1 - male, 2 - female.") 
	parser.add_argument("-model", "--model",action="store", type=str, dest="model", default="3", required=False, help="Model (test) number (see INTERSNP documentation).")
	parser.add_argument("-pheno", "--pheno",action="store", type=str, dest="pheno", required=True, help="Phenotype")
	parser.add_argument("-cov", "--covariates",action="store", type=str, dest="covariates", default="BirthCohort", required=False, help="A list of comma-separated covariates.")
	parser.add_argument("-fid", "--family",action="store", type=str, dest="familyID", default="fid", required=False, help="A family id in a phenotype file")
	parser.add_argument("-sid", "--subject",action="store", type=str, dest="subjectID", default="SubjID", required=False, help="A subject id in a phenotype file")
	parser.add_argument("-pfile", "--phenotype_file", action="store", type=str, dest="phenofile", default=None, required=True, help="A subject id in a phenotype file")
	parser.add_argument("-genes", "--candidate_genes", action="store", type=str, dest="cand_genes", default=None, required=False, help="A column name with candidate genes")
	parser.add_argument("-annfn", "--annotation_file", action="store", type=str, dest="annfn", default=None, required=True, help="Annotation file")
	parser.add_argument("-bfile", "--bfilepath", action="store", type=str, dest="bfile", default=None, required=True, help="A path to plink files")
	parser.add_argument("-scov", "--sexcov", action="store", type=str, dest="sexcov", default=None, required=False, help="A sex covariate")
	args = parser.parse_args()
	#-------------------------#
	
	sexstrat_list = ["TOTAL"]
	sexcov = args.sexcov
	if args.sexstrat == True :
		if sexcov == None :
			print "sexcov not defined. Exiting.."
			sys.exit()
		sexstrat_list = ["FEMALE", "MALE"]
		sexcov = sexcov.strip().lower()
		
	
	for sex in sexstrat_list :
		print "Sex:", sex
		project_dir = args.project_dir + "_" + sex
		data_dir = project_dir + "/Data/"
		results_dir = project_dir + "/Results/"
		models_dir = project_dir + "/Models/"
		pheno_filename = "pheno.txt"
		covariates_filename = "covariates.txt"
		selected_snps_filename = "selectedsnps.txt"
		individuals_filename = "individuals.txt"
	
		## Setting-up the project directories: # Need try-catch for this code block
		subprocess.call(["mkdir", project_dir])
		subprocess.call(["mkdir", data_dir])
		subprocess.call(["mkdir", results_dir])
		subprocess.call(["mkdir", models_dir])
		
		#Prepare phenotype
		prepPheno(args.phenofile, args.familyID, args.subjectID, args.pheno, args.covariates, data_dir + pheno_filename, data_dir + covariates_filename, data_dir + individuals_filename, sex, sexcov)
		
		#Select candidate SNPs (if needed)
		if	args.cand_genes != None :
			selectCandidateGenes(args.cand_genes, data_dir + selected_snps_filename, args.annfn)
			#Perform quality control with Plink
			plinkQC(args.bfile, data_dir + selected_snps_filename, data_dir + individuals_filename, data_dir + args.pname)
		
		# Make corrections in genetics: # Need try-catch for this code block
		runPlink(data_dir + args.pname + "_qc", data_dir + pheno_filename, data_dir + args.pname + "_qc.cor")
		
		# Prepare selection file
		selfname = ""
		if args.selfname == None : 
			print "Default selection strategy will be used." # Need try-catch for this code block
			selfname = models_dir + "selection.txt"
			fam = data_dir + args.pname +"_qc.cor.fam"
			bim = data_dir + args.pname +"_qc.cor.bim"
			bed = data_dir + args.pname +"_qc.cor.bed"
			covariatefile = data_dir + "covariates.txt"
			singlemarker = "1"
			twomarker = "1"
			test = args.model
			f = open(covariatefile, 'rU')
			lines = f.readlines()
			f.close()
			covariates = "1" + "-" + str(len(args.covariates.split(','))) + ";"
			snplist = args.snplist
			if snplist != None :
				subprocess.call(["cp", snplist, models_dir])
				snplist = models_dir + ntpath.basename(snplist)
			outputname = results_dir
			selection.prepSelectionFile(selfname, fam, bim, bed, covariatefile, singlemarker, twomarker, test, covariates, snplist, "1" if args.sexstrat == True else "0", outputname)
		else :
			print "Reading custom file..." # Need try-catch for this code block
			selfname = args.selfname
			fam = data_dir + args.pname +"_qc.cor.fam"
			bim = data_dir + args.pname +"_qc.cor.bim"
			bed = data_dir + args.pname +"_qc.cor.bed"
			covariatefile = data_dir + "covariates.txt"
			snplist = args.snplist
			if snplist != None :
				subprocess.call(["cp", snplist, models_dir])
				snplist = models_dir + ntpath.basename(snplist)
			outputname = results_dir
			selfname_out = models_dir + "selection.txt"
			selection.prepSelectionFile2(selfname, selfname_out, fam, bim, bed, covariatefile, snplist, outputname)
			
			print "Done."
		
		# Run INTERSNP
		runIntersnp(selfname)
		
		# Generate a final report:
		subprocess.call([curr_dir + "/src/generateReport", results_dir+"BestMarkerCombi2Details.txt", results_dir+"BestMarkerCombi2.txt", results_dir+"SinglemarkerTop.txt", results_dir])
		# Add variable descriptions:
		addDescriptionSheet(results_dir+"report.xlsx")
		
	
# Start-up code =========={{{
if __name__=="__main__":
    main()
#.........................}}}
